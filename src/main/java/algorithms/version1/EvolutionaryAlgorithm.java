package algorithms.version1;

import algorithms.Reporte;
import algorithms.ParentAlgorithm;
import problems.ProblemI;

import java.util.ArrayList;
import java.util.Random;

public class EvolutionaryAlgorithm extends ParentAlgorithm {

    public Reporte execute(String algorithm, String timeLimitInSeconds, Reporte reporte){
        ProblemI m = getMaxCut(algorithm);
        Optimization op = new Optimization(m);
        Boolean maxFitness = false;
        Double max = 0.0;
        ArrayList<Individual> population = generateInitialPopulation(m);
        Individual motherMax = null;
        Individual fatherMax = null;
        int rounds = 0;

        long start = System.currentTimeMillis();

        // Comezo do algoritmo ata que acada o fitness máximo
        while(!maxFitness) {
            rounds++;
            ArrayList<Individual> childs = new ArrayList<Individual>();

            for (int i = 0; i < (m.numberOfVariables()/2); i++) {
                // Eleximos dous pais ó azar
                Individual parent1 = population.get((int) Math.round(Math.random() * (m.numberOfVariables() - 1)));
                Individual parent2 = population.get((int) Math.round(Math.random() * (m.numberOfVariables() - 1)));

                Individual child = op.oGetNewChild(parent1, parent2); // Obtemos o novo individuo
                child.fitness = m.eval(child.chromosome); // Calculamos o seu fitness
                childs.add(child);

                if(child.fitness > max) { // if de control
                    max = child.fitness;
                    System.out.println("\t [Version 1] Novo máximo acadado " + max);
                }
                if (child.fitness >= m.getMaxFitness()) { // Se o individuo acadou o fitness máimo rematamos a execución do programa
                    maxFitness = true;
                    motherMax = parent1;
                    fatherMax = parent2;
                    break;
                }
            }
            if(!maxFitness) replacement(population, childs); // Substituímos os novos individuos por individuos con baixo fitness na poboación

            if ((System.currentTimeMillis() - start) > (Integer.parseInt(timeLimitInSeconds)*1000)) {
                System.out.println("\t [Version 1] Maximo tiempo alcanzado " + timeLimitInSeconds);
                long end = System.currentTimeMillis();
                double time = (double) ((end - start));
                reporte.addReporte(max, time, rounds, false);
                return reporte;
            }

        }
        long end = System.currentTimeMillis();
        double time = (double) ((end - start));
        reporte.addReporte(max, time, rounds, true);
        System.out.println("\t [Version 1] Maximo fitness alcanzado " + max);
        return reporte;
        //System.out.println("Acadado o máximo " + max + " en " + time + " milisegundos");
        //System.out.println("A nai tiña un fitness de " + motherMax.fitness);
        //System.out.println("O pai tiña un fitness de " + fatherMax.fitness);


    }

    /**
     * Xera un conxunto de individuos inciais con cromosomas random
     *  @param m Tipo de problema
     * */
    public static ArrayList<Individual> generateInitialPopulation(ProblemI m) {
        Random rand = new Random();
        ArrayList<Individual> population = new ArrayList<Individual>();
        int num = 0;
        while (num < m.numberOfVariables()) {
            num++;
            Individual eva = new Individual();
            for (int i = 0; i < eva.chromosome.length; i++) {
                eva.chromosome[i] = rand.nextBoolean();
            }
            eva.fitness = m.eval(eva.chromosome);
            population.add(eva);
        }

        return population;
    }

    /**
     * Substitúe os childs con maior fitness que os parents, para xerar unha nova poboación
     * composta polos individuos con maior fitness.
     *
     * @param parents Poboación actual
     * @param childs Novos individuos a introducir na poboación
    * */
    public static void replacement(ArrayList<Individual> parents, ArrayList<Individual> childs) {
        for (int numParent = 0; numParent < parents.size(); numParent++) {
            for (int numChild = 0;  numChild < childs.size(); numChild++) {
                if(parents.get(numParent).fitness < childs.get(numChild).fitness){
                    parents.set(numParent, childs.get(numChild));
                    childs.remove(numChild);
                    break;
                }
            }
        }
    }

}
