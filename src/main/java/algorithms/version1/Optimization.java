package algorithms.version1;

import problems.ProblemI;

/**
 * This class implements the children cells to substitute the mothers
 *
 * @author Juan C. Burguillo Rial
 * @version 1.0
 */
class Individual {
    boolean[] chromosome = new boolean[Optimization.oProblemI.numberOfVariables()];
    double fitness = 0;
}


/**
 * This class implements the cells (points) in this simulation
 *
 * @author Juan C. Burguillo Rial
 * @version 1.0
 */
public class Optimization {

    public static ProblemI oProblemI;

    //----------------------- Used for the offspring -----------------------------------
    protected boolean[] bmPoint;                            // The point that an individual contains
    protected boolean[] bmNewPoint;                         // New generated point
    protected double dValFun;                               // Functional value of that point
//----------------------- Used for the offspring -----------------------------------


    public Optimization(ProblemI p) {
        oProblemI = p;
    }


    public boolean[] bmGetPoint() {
        return bmPoint;
    }

    public void vUpdate() {
        bmPoint = bmNewPoint;
    }

    public double dGetValFun() {
        return dValFun;
    }


    /**
     * This method calls to the function evaluate with a point to determine the functional value
     */
    public double dCalcValFun(boolean[] bmPointAux) {
        return oProblemI.evaluate(bmPointAux);
    }


    /**
     * This method recombines and mutates a new child from two parents.
     *
     * @param parent1 First Parent individual
     * @param parent2 Second Parent individual
     */
    public Individual oGetNewChild(Individual parent1, Individual parent2) {
        int iCut1, iCut2;                               // Cut points to do the reproduction
        Individual oCellChild = new Individual();

        // 1. We choose two cut points
        iCut1 = (int) (Math.random() * oProblemI.numberOfVariables());
        do {
            iCut2 = (int) (Math.random() * oProblemI.numberOfVariables());
        } while (iCut1 == iCut2);

        if (iCut1 > iCut2) {
            int iAux = iCut1;
            iCut1 = iCut2;
            iCut2 = iAux;
        }

        // 2. oCellChild inherits the longest part from the best parent  (MINIMIZE)
        if ((iCut2 - iCut1) > oProblemI.numberOfVariables() / 2) {
            if (parent1.fitness > parent2.fitness) {
                System.arraycopy(parent1.chromosome, 0, oCellChild.chromosome, 0, parent1.chromosome.length);
                System.arraycopy(parent2.chromosome, iCut1 + 1, oCellChild.chromosome, iCut1 + 1, iCut2 - iCut1);
            } else {
                System.arraycopy(parent2.chromosome, 0, oCellChild.chromosome, 0, parent2.chromosome.length);
                System.arraycopy(parent1.chromosome, iCut1 + 1, oCellChild.chromosome, iCut1 + 1, iCut2 - iCut1);
            }

        } else {

            if (parent1.fitness > parent2.fitness) {
                System.arraycopy(parent2.chromosome, 0, oCellChild.chromosome, 0, parent2.chromosome.length);
                System.arraycopy(parent1.chromosome, iCut1 + 1, oCellChild.chromosome, iCut1 + 1, iCut2 - iCut1);
            } else {
                System.arraycopy(parent1.chromosome, 0, oCellChild.chromosome, 0, parent2.chromosome.length);
                System.arraycopy(parent2.chromosome, iCut1 + 1, oCellChild.chromosome, iCut1 + 1, iCut2 - iCut1);
            }
        }

        // 3. Mutation applied to oCellChild
        for (int j = 0; j < oProblemI.numberOfVariables(); j++) {
            if (Math.random() <= 1.0 / (double) oProblemI.numberOfVariables())
                if (oCellChild.chromosome[j]) oCellChild.chromosome[j] = false;
                else oCellChild.chromosome[j] = true;
        }

        return oCellChild;
    }


} // from the class

