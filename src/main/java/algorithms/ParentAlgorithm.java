package algorithms;

import problems.ProblemI;
import problems.optimization.integer.MAXCUT100;
import problems.optimization.integer.MAXCUT20_01;
import problems.optimization.integer.MAXCUT20_09;
import problems.optimization.integer.MAXCUT5;

public class ParentAlgorithm {

    public ProblemI getMaxCut(String maxcutname) {
       switch (maxcutname) {
           case "MAX5":
               return new MAXCUT5();
           case "MAX20_01":
               return new MAXCUT20_01();
           case "MAX20_09":
               return new MAXCUT20_09();
           case "MAX100":
               return new MAXCUT100();
       }
        return new MAXCUT100();
    }

}
