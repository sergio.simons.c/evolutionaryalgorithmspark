package algorithms;

public class Reporte {

    int eficienciaMejor;
    double fitnessMejor;
    double tiempoMejor;
    boolean esMaximo;

    double fitnessMedio;
    double eficacia;
    int ejecuciones;

    public Reporte(){
        eficienciaMejor = 0;
        fitnessMedio = 0;
        fitnessMejor = 0;
        eficacia = 0;
        ejecuciones = 0;
    }

    public void addReporte(double fitness, double time, int rondas, boolean optimo){
        if (fitness > fitnessMejor) {
            fitnessMejor = fitness;
            eficienciaMejor = rondas;
            tiempoMejor = time;
            esMaximo = optimo;
        }

        fitnessMedio = fitnessMedio + fitness;
        if(optimo) eficacia++;
        ejecuciones++;
    }

    @Override
    public String toString() {
        fitnessMedio = fitnessMedio/ejecuciones;
        eficacia = eficacia/ejecuciones;

        return ("\nEficiencia del Mejor: " + eficienciaMejor +
                "\nFitness del Mejor (ms): " + fitnessMejor +
                "\nTiempo del Mejor: " + tiempoMejor +
                "\nEl mejor es optimo: " + esMaximo +
                "\n\nFitness medio: " + fitnessMedio +
                "\nEficacia: " + eficacia);
    }

}
