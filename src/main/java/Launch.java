import algorithms.version1.EvolutionaryAlgorithm;
import algorithms.Reporte;
import org.apache.commons.cli.*;

public class Launch {

    /**
     * CommandLine.
     */
    private static CommandLine commandLine;

    /**
     * Cli Options.
     */
    private static Options clOptions = new Options();

    /**
     * Original arguments.
     */
    private static String[] arguments;


    public static void main(String... args) {
        arguments = args;

        clOptions = new Options();
        clOptions.addRequiredOption("t", "task", true, "The task you want to run.");
        clOptions.addRequiredOption("a", "algorithm", true, "Algorithm.");
        clOptions.addRequiredOption("n", "nodes", true, "Nodes.");
        clOptions.addRequiredOption("s", "time-limit", true, "time-limit.");
        clOptions.addRequiredOption("e", "algorithm-executions", true, "algorithm-executions.");

        // Parse options
        CommandLineParser parser = new DefaultParser();
        try {
            commandLine = parser.parse(clOptions, args, false);
        } catch (ParseException exception) {
            System.out.println("[ERROR] " + exception.getMessage());
        }

        // Parameters from arguments
        String task = commandLine.getOptionValue("task");
        String algorithm = commandLine.getOptionValue("algorithm");
        String nodes = commandLine.getOptionValue("nodes");
        String time = commandLine.getOptionValue("time-limit");
        String executions = commandLine.getOptionValue("algorithm-executions");

        if (task != null) {

            Reporte reporte = new Reporte();

            for(int x = 0; x < Integer.parseInt(executions); x++) {
                System.out.println("[INFO] Execution" + x);
                switch (task) {
                    case "version1":
                        (new EvolutionaryAlgorithm()).execute(algorithm, time, reporte);
                }
            }

            System.out.println("[FINAL RESULTS] " + reporte.toString());
        }
    }
}
