/**
 * @author Juan C. Burguillo
 * @version 1.0
 * 
 * Abstract class for defining prediction functions
 * 
 */
package problems;


public abstract class Function
{
protected int iNumInputVar;		  	// Number of input variables of the problem
protected double dIncSample;      	// Distance between successive samples
protected double dInitSample;     	// Initial point to sample

protected int iNumEval = 0; 	  	// Number of evaluations

public void vReset()
  {iNumEval = 0;}

public int igetNumEval()
  {return iNumEval;}

public int iGetNumberOfVariables()
{return iNumInputVar;}
  
public void vSetNumberOfVariables (int iNum)
{iNumInputVar = iNum;}

public double[] dmEvaluate () {
  double[] dOutput = dmEval ();        // Get the next value
  iNumEval++;                          // Increase the number of evaluations
  return dOutput;
}

public void vSetTraining () {}

public void vSetTesting () {}

public abstract double[] dmSetInput ();

public abstract double dSetOutput ();

public abstract double[] dmEval ();
}
