package problems.optimization.integer;
import problems.ProblemI;

public class MAXCUT5 extends ProblemI {

    public static final int longitCrom = 5; // Length of chromosomes
    public static final double maxFitness = 13.0; // Maximum Fitness Value


    final static double[][] problema= {

            {0.0, 1.0, 1.0, 1.0, 1.0},
            {1.0, 0.0, 0.0, 0.0, 10.0},
            {1.0, 0.0, 0.0, 0.0, 0.0},
            {1.0, 1.0, 0.0, 0.0, 0.0},
            {1.0, 1.0, 0.0, 0.0, 0.0},

    };

    int cols = 5;


    public MAXCUT5()    {
        super();
        variables = longitCrom;
        super.maxFitness = maxFitness;
    }


    public double eval(boolean[] ind)    {

        double fitness = 0.0;


        for (int i=0; i<(cols-1); i++) {
            for (int j=i; j<cols; j++) {
                if (ind[i]^ind[j]) {
                    fitness += problema[i][j];
                }
            }
        }

        return fitness;				// We negate it as EACO minimizes

    }

}
