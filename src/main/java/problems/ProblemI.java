
/**
 * @author Bernabe Dorronsoro
 *  *
 * Abstract class for defining problems
 * 
 */

package problems;


public abstract class ProblemI
{
protected int variables;			      // Number of variables of the problem
protected double maxFitness = 0.0;	// Optimal solution to the problem
protected int nEvals = 0;			      // Count of the number of evaluations

public void reset()
  {nEvals = 0;}

public int getNEvals()
  {return nEvals;}

public double evaluate(boolean[] ind) {
  double fit = eval(ind);     // get the fitness value
  nEvals++;                   // increase number of evaluations
  return fit;
}

public abstract double eval(boolean[] ind);
  
public int numberOfVariables()
{return variables;}
  
public void setNumberOfVariables(int num)
{variables = num;}
  
public void setMaxFitness(double num)
{maxFitness = num;}

public double getMaxFitness()
{return maxFitness;}
   
}
