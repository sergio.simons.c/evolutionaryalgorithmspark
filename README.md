De momento está cun algoritmo básico seguindo o esquema do enunciado e sen a configuración Sparkle (Ou sexa para executar no propio ordenador).

** Puntos a pensar:

- Xeración da poboación incial: Agora mesmo xeranse un número de individuos igual á lonxitude do cromosoma, polo tanto se estamos co problema      MAXCUT20_01 no que cada individuo ten un cromosoma de lonxitude 20, xeranse 20 individuos. ó mesmo tempo os cromosomas de cada individuo xeranse de manera aleatoria.

    - De que tamaño a de ser a poboación incial?
    - Os cromosomas inciais deben ser aleatorios?

- Elección de pais: Os dous pais seleccionados para xerar un fillo elixense de maneira aleatoria, polo tanto poden darse os seguintes casos:
    
    1. Os dous pais sexan o mesmo pai (Coñecido vulgarmente como o Candado Chino).
    2. Un pai podería ser pai de varios fillos.
    3. Dous pais poderían ter diferentes fillos, o que podería dar lugar a dous fillos iguais ainda que grazas a mutación podería ser que non.
    4. Pode haber pais sen descendencia.

    - Ten isto lóxica?

- Número de fillos creados: Creanse a metade de fillos que de pais, seguindo a lóxica de que por cada dous pais un fillo (Se hai 20 pais creanse 10 fillos).

    - Debemos crear máis fillos, menos?

- Tamaño nova Xeración: Unha vez creados os fillos, Os pais con un fitness menos que algún dos novos fillos remplazase polo fillo en cuestión (O pai sustituído non ten por que se o pai creador do fillo), mantendo así o mesmo número de individuos en todalas xeracións. Isto fai que as novas xeracións descarten aos pais e fillos de baixo fitness, quedansdose só cos mellores.

    - Ten isto lóxica?